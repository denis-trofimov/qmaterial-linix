import QtQuick 2.1
import QtQuick.Controls 1.3
import QtGraphicalEffects 1.0

Rectangle{
	id: root

	width: geometry.width + 10
	height: geometry.height + 10


	color: "#21bbd7" 

	property alias label: id_text.text
	property alias icon: img.source

	signal clicked()

	Item{
		anchors.centerIn: parent
		
		id: geometry
		width: img.width + 7 + id_text.width
		height: img.height + 7 + id_text.height

		Image {
			id: img
			source: "../icons/save.svg"				
			ColorOverlay {
	        	anchors.fill: img        
	        	color: "#ff0000"  // make image like it lays under red glass 
	    	}

	    	Text{
				id: id_text
			    text: "MY BUTTON"
			    color: "#babec1"
			    anchors.left: img.right		    
			}                          
		}
	}

	MouseArea{
		anchors.fill: parent		
		hoverEnabled: true

		onEntered: root.state = "hovered"
        onExited: root.state = ""


		onClicked: parent.clicked()
	}    

	states: [
		State{
			name: ""
			PropertyChanges{ target: root; color: "#21bbd7" }
			PropertyChanges{ target: id_text; color: "#ffffff" }			
		},
		State{
			name: "hovered"
			PropertyChanges{ target: root; color: "#ffffff" }
			PropertyChanges{ target: id_text; color: "#21bbd7" }
		}
		
	]
}