import QtQuick 2.0
import "../material"


import "tree_logic.js" as TL


Item {   
   id: root
   clip: true


   property var selected
   property string opened_path
   property var root_items


   Flickable {    
      id:  flick_panel
      anchors.fill: parent
      contentWidth: root.width
      contentHeight: clmn.height
      clip: true

      flickableDirection: Flickable.VerticalFlick
      boundsBehavior: Flickable.StopAtBounds

      Column{

         id: clmn
         width: root.width

         Repeater{
            id: items_repeater
            model:  root_items
            
            Loader{
              id: loader
              width: root.width
              
              property bool is_modified:  model.modelData.is_modified
              property string url:  model.modelData.url
              property var items:  model.modelData.items
              property string name:  model.modelData.name
              property bool is_dir:  model.modelData.is_dir
              property int level:  model.modelData.level
              property bool is_open:  false
              property bool is_selected: false
              property var find_last_opened_on_start: find_opened_node // передаём метод, который нужно вызвать, если путь полностью совпал
              
              property var onItemSelected: selectItemHandler              
              property var onSelect: selectItemHandler

              source: model.modelData.is_dir ? "FileTreeFolder.qml" : "FileTreeFile.qml"

              onStatusChanged: {
                if (loader.status == Loader.Ready) {                        
                  // Развернуть папки, если нужно
                  // if(opened_path){
                  //   TL.open_folders_for_path(loader, opened_path);                    
                  // }
                }
              }
            }
          }
      }
   }      

   onSelectedChanged: {
      // selectionChanged(selected.url)
      dispatcher.open_file(selected.url, 0)
    }


    ScrollBar{
      target: flick_panel
    }
   


    onOpened_pathChanged: {
      // раскрывает папки
      var i;
      for(i=0; i<items_repeater.count; i++){
        TL.open_folders_for_path(items_repeater.itemAt(i), opened_path);
      }
    }

    
    // функция вызывается когда найден конечный элемент раскрываемого пути
    function find_opened_node(item){
      // выделяем его
      selectItemHandler(item);
      
      // TODO: доскролить до найденного объекта
        // console.log("---11----->", flick_panel.mapFromItem(item.children[0], 0,0).y);
    }

   // функция вызывается при клике на файл (для изменения старого активного элемента на неактивный)
   function selectItemHandler(item){          
      item.is_selected = true
      if (selected && selected !== item){
        selected.is_selected = false
      }
      selected = item
  }

  

}