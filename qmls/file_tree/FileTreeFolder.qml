import QtQuick 2.0
import "../material"


Column{	
	id: root

	Item{
		width: root.width
		height: 48

		// Rectangle{
			// anchors.fill: parent
			// color: "#36434c"
			// visible : is_open
		// }

		Icon{
			color: p_800
			path: is_open ? "icons/menu_down.svg" : "icons/menu_right.svg"
			anchors.verticalCenter: parent.verticalCenter
			x: 18 * (level-1)
		}			
		Icon{
			x: 18 * level +6
			color: p1_400
			path: items.length > 0 ?  "icons/folder_black.svg" 	: "icons/folder.svg"
			anchors.verticalCenter: parent.verticalCenter
			opacity: 0.8
		}	    
		Text{
			x: 18 * (level + 1 ) + 18 + 2
			text: name
			color: "black"
			font.pixelSize: 13
			anchors.verticalCenter: parent.verticalCenter
			opacity: 0.7
		}			
		MouseArea{
			anchors.fill: parent
			onClicked: {
				is_open = ! is_open
				is_selected = true
			}
		}	
	}
	

	Column{
		visible: is_open
		Repeater{
			model: items
			objectName: "items_repeater"
			Loader{
				id: loader
				width: root.width
				source: model.modelData.is_dir ? "FileTreeFolder.qml" : "FileTreeFile.qml"

				property bool is_modified:  model.modelData.is_modified
				property string url:  model.modelData.url
				property var items:  model.modelData.items
				property string name:  model.modelData.name
				property bool is_dir:  model.modelData.is_dir
				property int level:  model.modelData.level
				property bool is_open:  false
				property bool is_selected: false
				property var onSelect: onItemSelected


				onStatusChanged: {
	                if (loader.status == Loader.Ready) {   
	                	// TODO: сделать раскрытие на старте (если нужно)
                		// if (opened_on_start){
						// }
	                }
         	     }
			}
		}
		
	}

}
