import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Dialog{
    id : root
    title: "Свойства"
    visible: true    
    height: parent.height - 40  
    hide_on_lost: false  

    // property var current_content : creations_content

    header: Tabs{            
        content:[
            TabItem{ title: "Button"; target: tab_content_1},
            TabItem{ title: "Cards";  active: true;  target: tab_content_2},
            TabItem{ title: "Dialog"; target: tab_content_3},
            TabItem{ title: "Confirm"; target: tab_content_10},
            TabItem{ title: "Alert"; target: tab_content_11},
            TabItem{ title: "Paper"; target: tab_content_4},
            TabItem{ title: "Navigator"; target: tab_content_5},
            TabItem{ title: "Lists"; target: tab_content_6},
            TabItem{ title: "Selections"; target: tab_content_7},
            TabItem{ title: "Shadow";  target: tab_content_8},
            TabItem{ title: "TextFields";  target: tab_content_9},
            TabItem{ title: "Divider";  target: tab_content_12},
            TabItem{ title: "Toolbar"; target: tab_content_13},
            TabItem{ title: "Texts"; target: tab_content_14},
            TabItem{ title: "Texts line height";  target: tab_content_15},
            TabItem{ title: "Tabs"; target: tab_content_16}
        ]
    }
    content :  Item{
        width: 500
        height: 500

        SampleButtons{
            id: tab_content_1
        } 
        SampleCards{
            id: tab_content_2
        }
        SampleDialog{
            id: tab_content_3
        }
        SamplePaper{
            id: tab_content_4
        }
        SampleNavigator{
            id: tab_content_5
        }
        SampleLists{
            id: tab_content_6
        }
        SampleSelections{
            id: tab_content_7
        }
        SampleShadow{
            id: tab_content_8
        }
        SampleTextFields{
            id: tab_content_9
        }
        SampleConfirm{
            id: tab_content_10
        }
        SampleAlert{
            id: tab_content_11
        }
        SampleDivider{
            id: tab_content_12
        }
        SampleToolbar{
            id: tab_content_13
        }
        SampleText{
            id: tab_content_14
        }
        SampleTextHeight{
            id: tab_content_15
        }
        SampleTabs{
            id: tab_content_16
        }
    }

    actions: Row{

    }

    MouseArea {
        // Эта штука нужна только для перехвата скролла
        // чтобы списки под этими элементами не скролились

        anchors.fill: parent
        acceptedButtons: Qt.MiddleButton | Qt.RightMouse
    }
}