import QtQuick 2.0

import "../material"
import "../editor_components"

Item{
	id: root
	width: 500		

	Card{
		width: 343
		icon: ""
		title: "Title"
		subhead: "Subhead"
		media:Image{
			width: 343
			height: 195
			source:"../material/imgs/card_picture.png"
		}
		description: Text{
			width: parent.width
			wrapMode: Text.Wrap
			text: 'Нужно бежать со всех ног, чтобы только оставаться на месте, а чтобы куда-то попасть, надо бежать как минимум вдвое быстрее!'
		}
		actions: [
			FlatButton{text: "Share"},
			FlatButton{text: "Action"}
		]
	}

	Card{
		x: 360
		width: 343

		media:Image{
			width: 343
			height: 195
			source:"../material/imgs/card_picture.png"
		}

		actions: [
			FlatButton{text: "Share"},
			FlatButton{text: "Action"}
		]
	}
	Card{
		y: 260
		x: 360
		width: 343

		media:Image{
			width: 343
			height: 195
			source:"../material/imgs/card_picture.png"
		}

		text: "Просто не знаю, кто я сейчас такая. Нет, я, конечно, примерно знаю, кто такая я была утром, когда встала, но с тех пор я всё время то такая, то сякая – словом, какая-то не такая."
	}

}