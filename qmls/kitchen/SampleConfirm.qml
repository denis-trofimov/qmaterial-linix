import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	width: 800		
	height: 800
	// anchors.fill: parent


	Item{
		y: 120
		height: 400
		width: root.width 
		
		Confirm{
			width: 56*5
			title: "Use Google`s location service?"			
			text: "Let Google help apps datermine location. This means sending anonymous location data to Google, even when no apps are running." 			
			onAccepted: console.log("AGREE")
			onRejected: console.log("DISAGREE")
		}
	}


}