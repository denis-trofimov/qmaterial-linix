import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	width: 500		
	Row{
		Column{
			width: 200
			Row{
				Checkbox{}
				Checkbox{ value: true }
				Checkbox{ disabled: true }
				Checkbox{ value: true; disabled: true }	
			}
			Rectangle{
				width: 200
				height: 40
				color: p_800
				Row{
					Checkbox{dark: true}
					Checkbox{ dark: true; value: true }
					Checkbox{ dark: true; disabled: true }
					Checkbox{ dark: true; value: true; disabled: true }	
				}
			}
	    }

	    Column{
			width: 200
			Row{
				Radio{}
				Radio{ value: true }
				Radio{ disabled: true }
				Radio{ value: true; disabled: true }	
			}
			Rectangle{
				width: 200
				height: 40
				color: p_800
				Row{
					Radio{dark: true}
					Radio{ dark: true; value: true }
					Radio{ dark: true; disabled: true }
					Radio{ dark: true; value: true; disabled: true }	
				}
			}
	    }
	    Column{
			width: 200
			Row{
				Switch{}
				Switch{ value: true }
				Switch{ disabled: true }
				Switch{ value: true; disabled: true }	
			}
			Rectangle{
				width: 200
				height: 40
				color: p_800
				Row{
					Switch{dark: true}
					Switch{ dark: true; value: true }
					Switch{ dark: true; disabled: true }
					Switch{ dark: true; value: true; disabled: true }	
				}
			}
	    }
	}

}