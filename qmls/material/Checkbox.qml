import QtQuick 2.0

Item{
	id: root

	property bool value: false
	property alias disabled: icon.disabled
	property bool dark: false

	width: 40
	height: 40
	
	IconButton{
		id: icon
		path: root.value ? "icons/checkbox-marked.svg" :"icons/checkbox-blank-outline.svg"
		color: root.value ? p1_500 : root.dark ? "white" :"black"
		icon_opacity: root.disabled ? 0.26 : root.value ? 1.0 : root.dark ? 0.7 : 0.54
		Behavior on color { ColorAnimation { duration: 100 } }		
		onClicked: root.value = !root.value
		
	}
}
