import QtQuick 2.0

Text{
	color: "black"
	font.family: "Roboto"
	opacity: 0.54
	font.pixelSize: 56
	font.letterSpacing: -1
	lineHeight: 1.0
	lineHeightMode: Text.ProportionalHeight
	textFormat: Text.PlainText
	wrapMode: Text.Wrap
	property string type: "Display3"
}

