import QtQuick 2.0

Item {
    id: root

    property var target
    property alias color: rct.color
    
    anchors.right: parent.right
    anchors.rightMargin: 4
    anchors.topMargin: 8
    anchors.bottomMargin: 8
    
    width: 6
	height: target.height

    Rectangle{
        color: "black"
        opacity: 0.1
        width: 6
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        visible: target.visibleArea.heightRatio < 1.0
    }

	Item{
		y: parent.anchors.topMargin

		Rectangle{
	    	id: rct
	    	color: "black"
	    	opacity: 0.24
	    	radius: 1
	    	width: 6
	    	y: target.visibleArea.yPosition * (root.height) - 8
	    	height: target.visibleArea.heightRatio * root.height
	    	visible: target.visibleArea.heightRatio < 1.0

	    	Behavior on opacity {
				id: behavior_fade
				SmoothedAnimation { velocity: 0.3 } 
			}
			onYChanged:{
		    	behavior_fade.enabled = false
		    	rct.opacity =  0.24   
		    	
		    	if (!mouse.containsMouse) {
		    		time_fade.restart()	
		    	}
		    }
	    }

	    Item{
	    	id: rct_active
	    	opacity: 0.24
	    	width: 6
	    	y: 0
	    	x: 40
	    	height: rct.height
	    	visible: false
	    }
	}
    

    MouseArea{
    	id: mouse
    	anchors.fill: parent
    	hoverEnabled: true

    	onEntered: {
    		behavior_fade.enabled = false
    		rct.opacity = 0.24
    	}
    	onExited : {
    		behavior_fade.enabled = true
    		time_fade.restart()
    	}
    }

    Item{
    	// color: "yellow"
    	width: 20
    	height: root.height
    	x: -10    	
    	y: root.anchors.topMargin
    	
    	MouseArea{
    		anchors.fill: parent
    		drag.target: rct_active
    		drag.axis: Drag.YAxis
    		drag.minimumY: 0
    		drag.maximumY: target.height - rct_active.height

    		onPressed:{
    			target.contentY = Qt.binding(calc_scroll)
    		}

    		onReleased:{
    			target.contentY = calc_scroll()
    		}
    	}
    }
    
    Timer {
    	id: time_fade
        interval: 1500; running: false; repeat: false
        onTriggered: {
        	behavior_fade.enabled = true
        	rct.opacity = 0.01
        }
    }
    function calc_scroll(){
		return (rct_active.y / (root.height-rct_active.height))* (target.contentHeight-target.height)
	}
}	

