import QtQuick 2.0
import "../material"

Item{			
	id: root	
	height: 48	
	
	// property var currentItem
	// property var items
	// property string name
	// property bool is_leaf
	// property int level
	// property bool is_open:  false
	// property bool is_selected: false
	// property var onSelect

	

	Rectangle{
		anchors.fill: parent
		color: is_selected ? type=="noun" ? "#8973b9" : p1_500 : p1_500
		visible : is_selected
	}

	Item{
		anchors.fill: root
		anchors.leftMargin: 18 * level + 6

		Icon{
			color: is_selected ? "white" : type=="noun" ? "#8973b9" : "black"
			path: is_highlighted ? "icons/hexagon.svg" : "icons/hexagon_outline.svg"
			anchors.verticalCenter: parent.verticalCenter
			opacity: 1
		}					

		Text{
			x: 18+18
			font.pixelSize: 13
			text: name
			color: is_selected ? "white" : is_highlighted ? type=="noun" ? "#8973b9" : p1_500  : "black"
			anchors.verticalCenter: parent.verticalCenter
			opacity: is_selected ? 1.0 : is_highlighted ? 1 : 0.7
		}
	}
	
	

	MouseArea{
		anchors.fill: parent
		onClicked: {
			// console.log(root.parent.is_selected)
			// root.parent.is_selected = true
			is_selected = true			
			dispatcher.navigation_node_selected(item_id);
			// onSelect(root.parent)
			// root.is_selected = true
			// console.log("--->>>", current)
		}
	}
}
